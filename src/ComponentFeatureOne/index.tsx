import React from 'react';
import './styles.scss';

interface IComponentProps {
  text: string;
}

export default ({ text }: IComponentProps) => (
  <p className="feature-one">{text}</p>
);
