# React Package Boilerlplate

Basic package boilerlplate for modular react npm-packages

## Steps to create a library:

1. Create modular app structure in *src*
1. All your components should have *.tsx* extension
1. Write some tests
1. Rename your library in **package.json** of your library
1. Update *repository* field in **package.json**
1. Add **package.json** files for parts of the app, which could be used separately to *parts* folder
1. For better experience, you should add root *index.ts* file, which will export all the parts
1. Update *README* to describe your package
1. Create some examples to show how your package can be used, to add library as a dependency for the example:
  1. Add `"library-name": "link:$relative-path-to-library"` to the dependencies of your example
  1. Run `yarn`

## Using library
You can import either whole library: `import lib from 'library-name;`, or a separated part of the library: `import FeatureName from 'library-name/parts/FeatureName';`

## Using scss:
You can use *scss* preprocessor for your components out of the box:

1. Use `import './style.scss';` in the components, which need to be styled
1. All styles will be bundeled to *css/main.css*
1. Include the library styles on client(for example if you use webpack for your client app):
  1. Add `@import(~package-name/css/index.css);` in main css file if you want to import all styles from the library
  1. Add `@import(~package-name/css/FeatureName/index.css);` in main css file if you want to import styles only for specific part of the library

## Adding envs to example:
Because *example* is based 0n *create-react-app* it is possible to use custom envs as suggested [here](https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables)

## Building:
Run `yarn build` to prepare your library to be deployed

## Releasing:
Run `yarn release:$type` to release your app (where **$type** can be: patch, minor or major)

## Development
Run `yarn start` to start your application

## Running test
Run `yarn test` to run tests once
Run `yarn test:watch` to rerun tests each time any file changes