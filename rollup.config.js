import fs from 'fs-extra';
import { resolve, join, dirname } from 'path';
import scss from 'rollup-plugin-scss';
import nodeResolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import tslint from 'rollup-plugin-tslint';
import typescript from 'rollup-plugin-typescript2';

const SOURCE_DIR = 'src';
const DEST_DIR = 'dist';
const CSS_DIR = 'css';
const ROOT_INDEX = resolve('index.ts');

const HAS_INDEX_REG_EXP = /index\.[tj]sx?$/;

// Compiling all parts

export default fs.readdirSync(SOURCE_DIR).map(entry => {
  let entryIndex = '';

  if (!HAS_INDEX_REG_EXP.test(entry)) {
    entryIndex = fs.readdirSync(resolve(SOURCE_DIR, entry)).find(file => HAS_INDEX_REG_EXP.test(file));
  }

  const entryPath = join(entry, entryIndex);
  
  const config = {
    input: resolve(SOURCE_DIR, entryPath),
    output: {
      format: 'es',
      file: resolve(DEST_DIR, entryPath.replace(/\.[tj]sx?$/, '.js')),
    },
    plugins: [
      scss({
         output: styles => {
           if (styles && styles.length > 0) {
            const path = resolve(CSS_DIR, entryPath.replace(/.tsx?$/, '.css'));

            fs.ensureDirSync(dirname(path));
            fs.writeFileSync(path, styles)
           }
        },
      }),
      tslint({
        include: ['**/*.ts', '**/*.tsx'],
      }),
      typescript({
        tsconfigOverride: {
          compilerOptions: {
            // Generate declaration only for root index file
            // Otherwise, we get a lot of duplicate .d.ts files and broken structure of dist
            declaration: resolve(entry) === ROOT_INDEX,
          }
        }
      }),
      peerDepsExternal(),
      nodeResolve({
        jsnext: true,
      }),
      commonjs(),
    ],
  };

  return config;
});
