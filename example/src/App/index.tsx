import React from 'react';

import { ComponentFeatureOne, featureOne } from 'interdan-react-package-boilerplate';
// import ComponentFeatureOne from 'interdan-react-package-boilerplate/parts/ComponentFeatureOne';
// import featureOne from 'interdan-react-package-boilerplate/parts/featureOne';

import './styles.scss';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <ComponentFeatureOne text={featureOne('user')} />
          {process.env.REACT_APP_TEST_ENV}
        </header>
      </div>
    );
  }
}

export default App;
